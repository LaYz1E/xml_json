package com.epam.XML;

import javax.xml.bind.*;
import javax.xml.namespace.QName;
import javax.xml.stream.*;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.util.ArrayList;
import java.util.List;


public class UnmarshalDemo {

    ArrayList parseXMLbyJAXB(String filename) {

        ArrayList products = new ArrayList();
        XMLInputFactory xif = XMLInputFactory.newFactory();
        StreamSource xml = new StreamSource(filename);
        XMLStreamReader xsr = null;

        try {
            xsr = xif.createXMLStreamReader(xml);
            xsr.next();
            JAXBContext jc = JAXBContext.newInstance(ProductWithStringDate.class);
            Unmarshaller unmarshaller = jc.createUnmarshaller();
            while (xsr.hasNext()) {
                int next = xsr.next();
                if ((next == XMLStreamConstants.START_ELEMENT || next == XMLStreamConstants.END_ELEMENT) && xsr.getLocalName().equals("Product")) {
                    JAXBElement<ProductWithStringDate> jb = unmarshaller.unmarshal(xsr, ProductWithStringDate.class);
                    ProductWithStringDate product = jb.getValue();
                    products.add(product);
                }
            }
            xsr.close();
        } catch (XMLStreamException | JAXBException e) {
            e.printStackTrace();
        }
        return products;
    }

    @Deprecated
    public static List<ProductWithStringDate> parseXMLbyStax(String filename) throws XMLStreamException {
        List<ProductWithStringDate> productsList = new ArrayList<ProductWithStringDate>();
        ProductWithStringDate product = null;
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLEventReader reader = null;
        try {
            reader = xmlInputFactory.createXMLEventReader(new FileInputStream(filename));
            while (reader.hasNext()) {
                XMLEvent xmlEvent = reader.nextEvent();
                if (xmlEvent.isStartElement()) {
                    StartElement startElement = xmlEvent.asStartElement();
                    if (startElement.getName().getLocalPart().equals("Product")) {
                        product = new ProductWithStringDate();
                        Attribute idAttr = startElement.getAttributeByName(new QName("id"));
                        if (idAttr != null) {
                            product.setId(Integer.parseInt(idAttr.getValue()));
                        }
                    } else if (startElement.getName().getLocalPart().equals("manufacturer")) {
                        xmlEvent = reader.nextEvent();
                        assert product != null;
                        product.setManufacturer(xmlEvent.asCharacters().getData());
                    } else if (startElement.getName().getLocalPart().equals("create_date")) {
                        xmlEvent = reader.nextEvent();
                        assert product != null;
                        product.setCreate_date(xmlEvent.asCharacters().getData());
                    } else if (startElement.getName().getLocalPart().equals("model")) {
                        xmlEvent = reader.nextEvent();
                        assert product != null;
                        product.setModel(ModelEnum.valueOf(xmlEvent.asCharacters().getData()));
                    } else if (startElement.getName().getLocalPart().equals("color")) {
                        xmlEvent = reader.nextEvent();
                        assert product != null;
                        product.setColor(xmlEvent.asCharacters().getData());
                    } else if (startElement.getName().getLocalPart().equals("price")) {
                        xmlEvent = reader.nextEvent();
                        assert product != null;
                        product.setPrice(Float.valueOf(xmlEvent.asCharacters().getData()));
                    }
                }
                if (xmlEvent.isEndElement()) {
                    EndElement endElement = xmlEvent.asEndElement();
                    if (endElement.getName().getLocalPart().equals("Product")) {
                        productsList.add(product);
                    }
                }
            }


        } catch (XMLStreamException | FileNotFoundException exc) {
            exc.printStackTrace();
        }

        return productsList;
    }

}