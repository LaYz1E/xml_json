package com.epam.XML;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;


@XmlRootElement(name = "Catalog")
@XmlAccessorType(XmlAccessType.FIELD)
public class Catalog {
    @XmlElement(name = "Product")
    private ArrayList<ProductWithStringDate> catalog = null;

    public ArrayList<ProductWithStringDate> getCatalog() {
        return catalog;
    }

    public void setCatalog(ArrayList<ProductWithStringDate> catalog) {
        this.catalog = catalog;
    }

    @Override
    public String toString() {
        return "Catalog{" +
                "catalog=" + catalog +
                '}';
    }
}
