package com.epam.XML;


import com.google.gson.Gson;
import jdk.internal.org.xml.sax.SAXException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;


public class Main {

    public static void main(String[] args) throws JAXBException, IOException {


        UnmarshalDemo unmarshalDemo = new UnmarshalDemo();

        JAXBContext jaxbContext = JAXBContext.newInstance(Catalog.class, ProductWithStringDate.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        Catalog catalog = new Catalog();
        catalog.setCatalog(unmarshalDemo.parseXMLbyJAXB("products.xml"));
        System.out.println("catalog from xml");
        System.out.println(catalog);

        Gson gson = new Gson();
        System.out.println("POJO to json");
        System.out.println(gson.toJson(catalog));

        System.out.println("catalog from json");
        Catalog catalogFromJson = gson.fromJson(gson.toJson(catalog),catalog.getClass());
        System.out.println(catalogFromJson);

        System.out.println("POJO to XML");
        try (FileWriter fileWriter = new FileWriter(System.getProperty("user.dir")
                + File.separator + "CreatedXML.xml")) {
            jaxbMarshaller.marshal(catalog, fileWriter);
            jaxbMarshaller.marshal(catalog, System.out);
        }


        try {
        ProductWithStringDate.validateXMLByXSD(System.getProperty("user.dir")
                    + File.separator + "products.xml", System.getProperty("user.dir")
                    +  File.separator + "products.xsd");
        } catch (SAXException e) {
            e.printStackTrace();
            System.out.println("validation is failed");
        }


    }




}



