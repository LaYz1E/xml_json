package com.epam.XML;

import jdk.internal.org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.bind.annotation.*;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.SchemaFactory;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;


@XmlRootElement(name = "Product")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductWithStringDate {
    @XmlAttribute
    private int id;
    private String manufacturer;
    private ModelEnum model;
    private String create_date;
    private String color;
    private float price;
    private String category;
    private String subcategory;

    public ProductWithStringDate() {
    }

    public static boolean validateXMLByXSD(String xml, String xsdPath) throws SAXException, IOException {
        try {
            SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI)
                    .newSchema(new File(xsdPath))
                    .newValidator()
                    .validate(new StreamSource(xml));
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }


    public String getCreate_date() {
        return create_date;
    }


    public void setCreate_date(String create_date) {
        this.create_date = create_date;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public ModelEnum getModel() {
        return model;
    }


    public void setModel(ModelEnum model) {
        this.model = model;
    }

    public String getColor() {
        return color;
    }


    public void setColor(String color) {
        this.color = color;
    }


    public float getPrice() {
        return price;
    }


    public void setPrice(float price) {
        this.price = price;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(String subcategory) {
        this.subcategory = subcategory;
    }

    @Override
    public String toString() {
        return "ProductWithStringDate{" +
                "id=" + id +
                ", manufacturer='" + manufacturer + '\'' +
                ", model=" + model +
                ", create_date='" + create_date + '\'' +
                ", color='" + color + '\'' +
                ", price=" + price +
                ", category='" + category + '\'' +
                ", subcategory='" + subcategory + '\'' +
                '}' + '\n';
    }
}
